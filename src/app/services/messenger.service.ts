import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessengerService {

  subject = new Subject()

  constructor() { }

  sendItem(product) {
    console.log(product)
    this.subject.next(product) //Event trigger
  }

  getItem() {
    return this.subject.asObservable()
  }
}
