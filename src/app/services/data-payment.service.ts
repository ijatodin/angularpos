import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataPaymentService {

  subject = new Subject()

  constructor() { }

  sendTotal(product) {
    console.log(product)
    this.subject.next(product) //Event trigger
  }

  getTotal() {
    return this.subject.asObservable()
  }
}
