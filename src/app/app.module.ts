import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './component/shared/navbar/navbar.component';
import { FooterComponent } from './component/shared/footer/footer.component';
import { HeaderComponent } from './component/shared/header/header.component';
import { ItemCartComponent } from './component/item-cart/item-cart.component';
import { ItemListComponent } from './component/item-cart/item-list/item-list.component';
import { CartComponent } from './component/item-cart/cart/cart.component';
import { CartItemComponent } from './component/item-cart/cart/cart-item/cart-item.component';
import { CheckOutPayComponent } from './component/check-out-pay/check-out-pay.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HeaderComponent,
    ItemCartComponent,
    ItemListComponent,
    CartComponent,
    CartItemComponent,
    CheckOutPayComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
