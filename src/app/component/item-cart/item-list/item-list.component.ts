import { Component, OnInit } from '@angular/core';
import * as data from './item-list.json';
import { MessengerService } from 'src/app/services/messenger.service'

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  products: any[] = (data as any).default;

  constructor(private msg: MessengerService) { }

  ngOnInit(){
    console.log(data);
  }

  addToCart(data) {
    this.msg.sendItem(data)
  }

}
