import { Component, OnInit } from '@angular/core';
import { MessengerService } from 'src/app/services/messenger.service';
import { DataPaymentService } from 'src/app/services/data-payment.service';
import { CheckOutPayComponent} from 'src/app/component/check-out-pay/check-out-pay.component'
import * as data from 'src/app/component/item-cart/item-list/item-list.json';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  // products: any[] = (data as any).default;

  cartItems = [
    // {id: 1, productId: 1, productName: 'Roti', qty: 3, price: 3.50 },
    // {id: 2, productId: 2, productName: 'Rotii', qty: 4, price: 2.50 },
    // {id: 3, productId: 3, productName: 'Rotiii', qty: 1, price: 3.50 },
  ];

  cartTotal = 0

  constructor(private msg: MessengerService, private dps: DataPaymentService,) { }

  ngOnInit() {

    this.msg.getItem().subscribe(product => {
      console.log(product);
      this.addItemToCart(product)
    })

  }

  addItemToCart(product){
    let productExists = false

    for (let i in this.cartItems) {
      if (this.cartItems[i].productId === product.id) {
        this.cartItems[i].qty++
        productExists = true
        break;
      }
    }

    if (!productExists) {
      this.cartItems.push({
        productId: product.id,
        productName: product.name,
        qty: 1,
        price: product.price
      })
    }

    this.cartTotal = 0
    this.cartItems.forEach(item => {
      this.cartTotal += (item.qty * item.price)
    })

  }

  clearCart() {
    this.cartItems = []
  }

  checkOut() {
    document.getElementById("overlay").style.display = "block";
    this.dps.sendTotal(this.cartTotal)
  }

  closeWindow() {
    document.getElementById("overlay").style.display = "none";
    this.clearCart();
  }

}
