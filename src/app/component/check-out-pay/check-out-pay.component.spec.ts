import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckOutPayComponent } from './check-out-pay.component';

describe('CheckOutPayComponent', () => {
  let component: CheckOutPayComponent;
  let fixture: ComponentFixture<CheckOutPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckOutPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckOutPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
