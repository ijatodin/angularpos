import { Component, OnInit } from '@angular/core';
import { DataPaymentService } from 'src/app/services/data-payment.service';

@Component({
  selector: 'app-check-out-pay',
  templateUrl: './check-out-pay.component.html',
  styleUrls: ['./check-out-pay.component.css']
})
export class CheckOutPayComponent implements OnInit {

  currentNumber = '0';
  firstOperand = null;
  operator = null;
  waitForSecondNumber = false;
  cartData: any = '0'

  constructor( private dps: DataPaymentService ) { }

  ngOnInit() {
    this.dps.getTotal().subscribe(product => {
      console.log(product);
      this.cartData = product
    })
  }

  public getNumber(v: string){
    console.log(v);
    if(this.waitForSecondNumber)
    {
      this.currentNumber = v;
      this.waitForSecondNumber = false;
    }else{
      this.currentNumber === '0'? this.currentNumber = v: this.currentNumber += v;

    }
  }

  public getNumberCart(cartData){
    console.log(cartData);
    if(this.waitForSecondNumber)
    {
      this.currentNumber = cartData;
      this.waitForSecondNumber = false;
    }else{
      this.currentNumber === '0'? this.currentNumber = cartData: this.currentNumber += cartData;

    }
  }

  getDecimal(){
    if(!this.currentNumber.includes('.')){
        this.currentNumber += '.';
    }
  }

  private doCalculation(op , secondOp){
    switch (op){
      case '+':
      return this.firstOperand += secondOp;
      case '-':
      return this.firstOperand -= secondOp;
      case '*':
      return this.firstOperand *= secondOp;
      case '/':
      return this.firstOperand /= secondOp;
      case '=':
      return secondOp;
    }
  }
  public getOperation(op: string){
    console.log(op);

    if(this.firstOperand === null){
      this.firstOperand = Number(this.currentNumber);
      console.log(this.firstOperand);

    }else if(this.operator){
      const result = this.doCalculation(this.operator , Number(this.currentNumber))
      this.currentNumber = String(result.toFixed(2));
      this.firstOperand = result;
    }
    this.operator = op;
    this.waitForSecondNumber = true;

    console.log(this.firstOperand);

  }

  registerPay() {
    const rst = Number(this.currentNumber) - this.cartData
    this.currentNumber = String(rst.toFixed(2))
  }

  clear(){
    this.currentNumber = '0';
    this.firstOperand = null;
    this.operator = null;
    this.waitForSecondNumber = false;
  }


}
